#!/usr/bin/env python

import json
import os
from random import shuffle

class Cards:
  # Create new card set
  def newCards(self):
    self.f = open("cardsets/%s" % self.name, "w")

    #Determine number of flash cards to be made
    valid = False
    while not valid:
        try:
            quantity = int(raw_input("How many flash cards do you wish to make?: "))
            if quantity < 0:
              raise ValueError
            valid = True
        except ValueError:
            print "Invalid response. Please enter a nonnegative integer."

    #Delete file if no flash cards and quit
    if quantity == 0:
      print "No cards created. Aborting."
      self.f.close()
      os.remove("cardsets/%s" % self.name)
      return False
    else:
      #Enter data
      card_front = []
      card_back = []
      for x in xrange(quantity):
          print "Card %d:" % (x + 1)
          card_front.append(raw_input("    German:  "))
          card_back.append(raw_input("    English: "))

      #Construct dictionary
      cards = dict.fromkeys(card_front)
      index = 0
      for key in card_front:
          cards[key] = card_back[index]
          index += 1

      #Save to file
      json.dump(cards, self.f)
      self.f.close()
      return True

  def __init__(self, name):
    self.name = name
    self.success = True
    
    #Check if cards file needs to have data entered 
    if not os.path.isfile("cardsets/%s" % self.name):
      self.success = self.newCards()

    if self.success:
      #Open cards file and extract dictionary
      self.f = open("cardsets/%s" % self.name, "r")
      self.cards = json.load(self.f)
      self.f.close()
      print "\nDone initializing card set\n"

  #Test card set
  def test(self):
    total_correct = 0.0
    total = len(self.cards)
    keys = self.cards.keys()
    shuffle(keys)
    valid = False

    #Choose language to test
    while not valid:
      side = raw_input("\nGerman or English questions? ").lower()
      valid = (side == "german" or side == "english" or side == "e" or side == "g")
      if not valid:
        print "Invalid response. Please choose German or English."

    #Page break (to hide previous answers)
    print "\n" * 30 

    #Go through card set
    for card in keys:
      if side == "german" or side == "g":
        question = card
        answer = self.cards[card]
      else:
        question = self.cards[card]
        answer = card
      print question + ": "
      response = raw_input().lower()
      if response == answer.lower():
          print "correct!\n"
          total_correct += 1.0
      else:
          print "incorrect. the answer was %s.\n" % answer

    #Print stats
    percentage_correct = str(total_correct / total * 100.0) + "%"
    print "\nyou got %d answers correct out of %d for a percentage of %s." % (total_correct, total, percentage_correct)

#Main flash card testing

quit = False
while not quit:
  #Choose card set
  available = "    ".join(os.listdir("cardsets/"))
  print "\nAvailable card sets:\n" + available 
  name = raw_input("\nEnter the name of the card set or \"quit\" to exit: ")
  if name.lower() == "quit":
    quit = True
  elif name == "":
    print "Please enter a name of a card set."
  else:
    #Choose whether to test or not
    card_set = Cards(name)
    valid = False
    while not valid and card_set.success:
      response = raw_input("Test the %s cardset (yes/no)? " % name).lower()
      if response == "yes" or response == "y":
        card_set.test()
        valid = True
      elif response == "no" or response == "n":
        print "aborting"
        valid = True
      else:
        print "Invalid response. Please enter yes or no."
